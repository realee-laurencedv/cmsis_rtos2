# CMSIS RTOS 2

We are not the author of this code, this repo exist only to provide git access to a small portion of code (without all the cmsis components), namely: RTX 5 RTOS source supplied by [ARM](https://developer.arm.com/tools-and-software/embedded/cmsis)  
See [original repo](https://github.com/ARM-software/CMSIS_5) for more info  
Contact [maintainer][maintainer_email] for information and/or questions


## Deployment

1. Add to **include** path of compiler/assembler/etc the _inc_ folder
2. Add to **compile** path of compiler the _src_ folder
3. **Selectively ignore** your choice of [_generic timer_](/src/os_tick_gtim.c), [_private timer_](/src/os_tick_ptim.c) or [_cputimer_](/src/os_systick.c) implementation, only one is supported at the same time.
4. Ensure all **dep are present** in the parent project

    - [CMSIS Core][cmsiscorerepo_link]


[maintainer_email]:     <MAILTO:laurencedv@realee.tech>
[cmsiscorerepo_link]:	https://gitlab.com/real-ee/public/cmsis_core
